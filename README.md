![Repo header image](http://itsalec.co.uk/images/placid-github-header.jpg)  

Placid is a Craft plugin which makes it easy to use REST services in your twig templates, whether thats getting a twitter feed, showing off dribbble shots or getting the weather, Placid has you covered.

**Given the announcement of Craft 3, Placid 2 will wait and during this time I will be working on bringing this version of Placid up to scratch, stay tuned!**

#### `1.3.6` is released

`1.3.6` Sees some template fixes in the backend and fixes some bugs people might have encountered from upgrading  

This now has support for the latest version of the OAuth plugin from Dukt, as of testing `0.9.71` is working. But any issues just give us a shout! [You can get the latest release here](https://github.com/alecritson/Placid/releases/latest)

**Support for custom headers**  
You can also now set your own headers to be sent with the request.

**Custom dashboard widgets**  
You can now create your own dashboard widgets based on requests you have set up. [See how its done](https://github.com/alecritson/Placid/wiki/Widgets)

## Installing / Updating

**Installing**
- [Download the latest release](https://github.com/alecritson/Placid/archive/v1.2.5.zip) and unzip
- Upload the placid directory to craft/plugins
- Install in admin area

**Updating**
- [Download the latest release](https://github.com/alecritson/Placid/archive/v1.2.5.zip) and unzip
- Replace placid directory in craft/plugins
- Refresh admin section and run the update

For full instructions on how to use, [refer to the wiki](https://github.com/alecritson/placid/wiki)

For changes/update information, its all on the [project page of my site](http://itsalec.co.uk/projects/placid)
